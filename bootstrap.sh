#!/usr/bin/env bash

LSB_RELEASE="$(which lsb_release)"
if [ -z "$LSB_RELEASE" ]; then
  echo "lsb_release binary is necessary."
  echo "You can install it with package lsb-core (Ubuntu) or lsb-release (Arch)"
  exit 1
fi

set -e


distrib="$($LSB_RELEASE -si)"
echo "Bootstraping PC for $distrib"

common_packages="git"

case "$distrib" in
  Ubuntu)
    install="apt install"
    packages="$common_packages pipenv"
    ;;
  Arch)
    install="pacman -S --noconfirm"
    packages="$common_packages python-pipenv"
    ;;
  *)
    echo "Distribution $distrib not supported."
    exit 1
    ;;
esac

set -x

sudo $install $packages
mkdir -p $HOME/Projects

cd $HOME/Projects

if [ ! -d "$HOME/Projects/ansible-pc" ]; then
  git clone git@framagit.org:Nautigsam/ansible-pc
fi

cd $HOME/Projects/ansible-pc

pipenv install
pipenv run ansible-galaxy install -r requirements.yml
pipenv run ansible-playbook -Ki inventory.local.yml install.yml
